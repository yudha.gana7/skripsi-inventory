<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_masuk', function (Blueprint $table) {
            $table->increments('id_barang_masuk');
            $table->string('no_barang_masuk');
            $table->integer('id_nama_barang')->reference('id_nama_barang')->on('nama_barang');
            $table->integer('stok_barang');
            $table->integer('id_satuan')->reference('id_satuan')->on('satuan');
            $table->integer('id_user')->reference('id_user')->on('user');
            $table->integer('harga_barang');
            $table->integer('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_masuk');
    }
};
