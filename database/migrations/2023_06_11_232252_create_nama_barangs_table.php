<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nama_barang', function (Blueprint $table) {
                $table->increments('id_nama_barang');
                $table->string('nama_barang');
                // $table->integer('id_satuan')->reference('id_satuan')->on('satuan');
                $table->integer('id_kategori')->references('id_kategori')->on('kategori');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nama_barang');
    }
};
