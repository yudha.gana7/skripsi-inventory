<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->increments('id_barang');
            $table->integer('id_nama_barang')->reference('id_nama_barang')->on('namabarang');
            $table->integer('stok_barang');
            $table->text('keterangan_penyimpanan')->nullable();
            $table->date('lama_penyimpanan');
            $table->integer('harga_barang');
            $table->integer('id_satuan')->reference('id_satuan')->on('satuan');
            $table->integer('id_user')->reference('id_user')->on('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
};
