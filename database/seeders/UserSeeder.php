<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
//faker
class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
  public function run()
  {
    $faker = Faker::create('id_ID');

    // for ($i = 1; $i <= 2; $i++) {

      DB::table('user')->insert([
        'name' => 'yudha administrator',
        'nomor_hp' => $faker->phoneNumber,
        'alamat' => $faker->address,
        'username'=>'yudhaadmin',
        'password' => bcrypt('12345'),
        'level' => 'administrator',
        // 'remember_token' => 'sgsgdes3232'
      ]);
      DB::table('user')->insert([
        'name' => 'yudha pegawai',
        'nomor_hp' => $faker->phoneNumber,
        'alamat' => $faker->address,
        'username'=>'yudhapegawai',
        'password' => Hash::make('12345'),
        'level' => 'pegawai',
        // 'remember_token' => 'sgsgdes3232'
      ]);

    // }
  }
}
