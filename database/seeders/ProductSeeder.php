<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


use Faker\Factory as Faker;
//faker
class ProductSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
  public function run()
  {
    $faker = Faker::create('id_ID');

    for ($i = 1; $i <= 15; $i++) {

      DB::table('barang')->insert([
        'id_nama_barang' => $faker->randomElement(['Gulamu', 'Telur Ayam Lestari', 'Kopi Cba', 'Kopi Bad Day', 'Sari Nutri', 'Ice Pop', 'Goldking', 'Catbury Diary Goat Milk', 'Taroh Net', 'Chitatos', 'Marjan Lebaran', 'Kecap Bang O', 'Kecap Cba', 'Saos Cba', 'Saos Del Monte', 'Tepung Beras Rose Brand', 'Tepung Terigu Kotak Merah', 'Teh Cap Sepeda Tidak Balap', 'Teh Cap Bukan Dandang', 'Kecap Bagong']),
        'stok_barang' => $faker->numberBetween(1,200),
        'id_kategori' => $faker->randomElement(['1', '2', '3', '4', '5']),
        'harga_barang' => $faker->numberBetween(10000,500000),
        'lama_penyimpanan' => $faker->dateTimeBetween($startDate='now', $endDate="+2 years"),
        // 'keterangan_penyimpanan' => $faker->randomElement(['1', '2', '3', '4', '5']),
        'id_user' => $faker->randomElement(['1', '2', '3', '4']),
        'id_satuan' => $faker->randomElement(['1', '2']),
      ]);

    }
  }
}
