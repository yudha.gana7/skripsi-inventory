<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class SatuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
  {
    $faker = Faker::create('id_ID');

    for ($i = 1; $i <= 5; $i++) {

      DB::table('satuan')->insert([
        'satuan_barang' => $faker->randomElement(['liter', 'kilogram', 'ikat', 'karung', 'botol']),
        // 'keterangan_satuan' => $faker->randomElement(['1', '2', '3', '4', '5']),
      ]);

    }
  }
}
