<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
  {
    $faker = Faker::create('id_ID');

    for ($i = 1; $i <= 5; $i++) {

      DB::table('kategori')->insert([
        'nama_kategori' => $faker->randomElement(['Minuman Bubuk', 'Bumbu Bubuk', 'Bumbu Cair', 'Sayuran', 'Buah']),
        // 'keterangan_kategori' => 'ini merupakan keterangan',
      ]);

    }
  }
}
