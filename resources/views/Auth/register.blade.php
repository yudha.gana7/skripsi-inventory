<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <title>Inventory Jago Sore | Register</title>
</head>

<body class="background-auth">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <img class="col-lg-5 d-none d-lg-block" src={{asset("image/jagoSoreLogo.png")}}>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Buat Akun Baru</h1>
                            </div>
                            <form class="user" action="{{ route('postRegister') }}" method="post">
                                {{ csrf_field() }}
                                <div class="col mb-3">
                                    <input type="text"
                                        class="form-control form-control-user @error('name') is-invalid @enderror"
                                        name="name" id="Nama" placeholder="Nama Lengkap" autofocus
                                        value="{{old('name')}}">
                                    @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col mb-3">
                                    <input type="text"
                                        class="form-control form-control-user @error('nomor_hp') is-invalid @enderror"
                                        name="nomor_hp" id="Nomor Telefon" placeholder="Nomor Telefon"
                                        value="{{old('nomor_hp')}}">
                                    @error('nomor_hp')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col mb-3">
                                    <input type="username"
                                        class="form-control form-control-user @error('username') is-invalid @enderror"
                                        name="username" id="Username" placeholder="Username"
                                        value="{{old('username')}}">
                                    @error('username')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col mb-3">
                                    <input type="textarea"
                                        class="form-control form-control-user @error('alamat') is-invalid @enderror"
                                        name="alamat" id="Alamat" placeholder="Alamat Tinggal"
                                        value="{{old('alamat')}}">
                                    @error('alamat')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col mb-3">
                                    <input type="password"
                                        class="form-control form-control-user @error('password') is-invalid @enderror"
                                        name="password" id="password" placeholder="Password"
                                        value="{{old('password')}}">
                                    @error('password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <span class="fa fa-eye-slash form-control-feedback view_password"></span>
                                </div>
                                <div class="col mb-3">
                                    <input type="password"
                                        class="form-control form-control-user @error('password_confirmation') is-invalid @enderror"
                                        name="password_confirmation" id="password_confirmation"
                                        placeholder="Repeat Password" value="{{old('password_confirmation')}}">
                                    @error('password_confirmation')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    {{-- <span class="fa fa-eye-slash form-control-feedback view_password"></span> --}}
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">Daftar</button>
                            </form>
                            <hr>
                            {{-- <div class="text-center">
                                <a class="small" href="forgot-password.html">Forgot Password?</a>
                            </div> --}}
                            <div class="text-center">
                                <a class="small" href="{{ url('/login') }}">Sudah Memiliki Akun? Silahkan Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    @include('layouts.script')


</body>

</html>