<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <title>Inventory Jago Sore | Login</title>
</head>

<body class="background-auth">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <img class="col-lg-6 d-none d-lg-block" src={{asset("image/jagoSoreLogo.png")}}>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Selamat Datang</h1>
                                    </div>
                                    <form class="user" action="{{route('postlogin')}}" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="username"
                                                class="form-control form-control-user @error('username') is-invalid @enderror"
                                                name="username" id="exampleInputUsername" aria-describedby="emailHelp"
                                                placeholder="Masukkan Username" value="{{old('username')}}">
                                            @error('username')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="password"
                                                class="form-control form-control-user @error('password') is-invalid @enderror"
                                                name="password" id="password" placeholder="Masukkan Password"
                                                value="{{old('password')}}">
                                            @error('password')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                            <span
                                                class="fa fa-eye-slash form-control-feedback view_password_login"></span>
                                        </div>
                                        {{-- <div class="form-group">
                                            <input type="password"
                                                class="form-control form-control-user @error('password') is-invalid @enderror"
                                                name="password" id="exampleInputPassword"
                                                placeholder="Masukkan Password">
                                            @error('password')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div> --}}
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block">Masuk</button>
                                    </form>
                                    <hr>
                                    {{-- <div class="text-center">
                                        <a class="small" href="forgot-password.html">Forgot Password?</a>
                                    </div> --}}
                                    <div class="text-center">
                                        <a class="small" href="{{ url('/register')}}">Belum Punya Akun?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>


    <!-- Bootstrap core JavaScript-->
    @include('layouts.script')

</body>

</html>