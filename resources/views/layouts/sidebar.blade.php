<ul class="navbar-nav bg-info sidebar sidebar-dark accordion mt-3 ml-3" id="accordionSidebar">

    <!-- Sidebar Toggler (Sidebar) -->
    {{-- <div class="text-center d-none d-md-inline mt-2">
        <button class="border-0" id="sidebarToggle"></button>
    </div> --}}

    <!-- Sidebar - Brand -->
    {{-- <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <i><img class="sidebarLogo" src={{asset("image/jagoSoreLogo.png")}}></i>
        <div class="sidebar-brand-text mx-3">Jago Sore</div>
    </a> --}}

    <div class="sidebar-brand  d-flex align-items-center justify-content-center mt-2">
        <img class="sidebarLogo rounded-circle" src={{asset("image/jagoSoreLogo.png")}}>
    </div>
    <div class="sidebar-brand  d-flex align-items-center justify-content-center mt-3" id="profileId">
        <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{auth()->user()->name}}
            <br>
            <small>{{ auth()->user()->level }}</small>
        </span>
    </div>


    <!-- Divider -->
    {{--
    <hr class="sidebar-divider  mt-0 mb-0"> --}}

    <div class="sidebar-heading mt-4">
        Navigasi
    </div>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ url('/dashboard')}}">
            <i class="fas fa-fw fa-home"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    {{--
    <hr class="sidebar-divider  mt-0 mb-0"> --}}

    <!-- Heading -->
    <!-- <div class="sidebar-heading">
        Semua Data
    </div> -->

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cubes"></i>
            <span>Data Master</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <!-- <h6 class="collapse-header">Custom Components:</h6> -->
                <a class="collapse-item" href="{{ url('/product')}}">Data Bahan Makanan</a>
                @if (auth()->user()->level=="administrator")
                <a class="collapse-item" href="{{ url('/namabarang')}}">Nama Barang</a>
                <a class="collapse-item" href="{{ url('/kategori')}}">Data Kategori</a>
                <a class="collapse-item" href="{{ url('/satuan')}}">Data Satuan Berat</a>
                @endif
                {{-- <a class="collapse-item" href="{{ url('/kategori')}}">Data Kategori</a>
                <a class="collapse-item" href="{{ url('/satuan')}}">Data Satuan Berat</a> --}}
            </div>
        </div>
    </li>

    <!-- Divider -->
    {{--
    <hr class="sidebar-divider  mt-0 mb-0"> --}}

    <!-- Heading -->
    <!-- <div class="sidebar-heading">
        Semua D
    </div> -->

    <!-- Nav Item - Pages Collapse Menu -->
    @if (auth()->user()->level=="administrator")
    <div class="sidebar-heading">
        Transaksi
    </div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-fw fa-dolly-flatbed"></i>
            <span>Transaksi</span>
        </a>
        <div id="collapseOne" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <!-- <h6 class="collapse-header">Custom Components:</h6> -->
                <a class="collapse-item" href="/transaksi-msk">Data Barang Masuk</a>
                <a class="collapse-item" href="/product">Data Barang Keluar</a>
            </div>
        </div>
    </li>
    @endif

    <!-- Divider -->
    {{--
    <hr class="sidebar-divider mt-0 mb-0"> --}}

    <!-- Heading -->
    <!-- <div class="sidebar-heading">
        Data Kategori
    </div> -->

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Laporan</span>
        </a>
        <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <!-- <h6 class="collapse-header">Custom Components:</h6> -->
                <a class="collapse-item" href="/laporan/stok">Stok Barang</a>
                <a class="collapse-item" href="/product">Barang Masuk</a>
                <a class="collapse-item" href="/product">Barang Keluar</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    @if (auth()->user()->level=="administrator")
    {{--
    <hr class="sidebar-divider  mt-0 mb-0"> --}}

    <div class="sidebar-heading">
        Manajemen
    </div>

    <li class="nav-item">
        <a class="nav-link" href="{{ url('/user')}}">
            <i class="fas fa-fw fa-id-card-alt"></i>
            <span>Manajemen Pengguna</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ url('/logActivity')}}">
            <i class="fas fa-fw fa-history"></i>
            <span>Manajemen Log Aktivitas</span>
        </a>
    </li>
    @endif



    <!-- Divider -->
    {{--
    <hr class="sidebar-divider d-none d-md-block"> --}}



    <!-- Sidebar Message -->
    <!-- <div class="sidebar-card d-none d-lg-flex">
        <img class="sidebar-card-illustration mb-2" src="img/undraw_rocket.svg" alt="...">
        <p class="text-center mb-2"><strong>SB Admin Pro</strong> is packed with premium features, components, and more!</p>
        <a class="btn btn-success btn-sm" href="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!</a>
    </div> -->

</ul>