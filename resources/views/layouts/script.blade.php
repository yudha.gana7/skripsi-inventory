<!-- Bootstrap core JavaScript-->
<script src="{{asset('template/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('template/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('template/js/sb-admin-2.min.js')}}"></script>

<!-- Page level plugins -->
<script src="{{asset('template/vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('template/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('template/js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('template/js/demo/chart-pie-demo.js')}}"></script>
<script src="{{asset('template/js/demo/datatables-demo.js')}}"></script>

<script>
  $('.view_password').on('click', function(){
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
      $(this).removeClass('fa-eye-slash');
      $(this).addClass('fa-eye');
   } else {
     x.type = "password";
     $(this).removeClass('fa-eye');
     $(this).addClass('fa-eye-slash');
  }
  var x = document.getElementById("password_confirmation");
    if (x.type === "password") {
      x.type = "text";
      $(this).removeClass('fa-eye-slash');
      $(this).addClass('fa-eye');
   } else {
     x.type = "password";
     $(this).removeClass('fa-eye');
     $(this).addClass('fa-eye-slash');
  }
});
</script>
{{-- <script>
  $('.view_password').on('click', function(){
    var x = document.getElementById("password_confirmation");
    if (x.type === "password") {
      x.type = "text";
      $(this).removeClass('fa-eye-slash');
      $(this).addClass('fa-eye');
   } else {
     x.type = "password";
     $(this).removeClass('fa-eye');
     $(this).addClass('fa-eye-slash');
  }
});
</script> --}}