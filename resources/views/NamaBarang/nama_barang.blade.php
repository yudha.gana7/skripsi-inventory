<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <title>Inventory Jago Sore | Data Kategori</title>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Data Kategori</h1>
                    <p class="mb-4">Data wajib diupdate secara berkala agar tidak terjadi kesalahan data
                        <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"></div>
                                <div class="col-sm-12 col-md-6">
                                    <a href="{{ url('/namabarang/tambah')}}"
                                        class=" btn btn-sm btn-primary shadow-sm float-right mb-3"><i
                                            class="fas fa-plus-square fa-sm text-white-50"></i> Tambah Barang Baru</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            {{-- <th>Satuan Berats</th> --}}
                                            <th>Keterangan Kategori</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            {{-- <th>Satuan Berats</th> --}}
                                            <th>Keterangan Kategori</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($namabarang as $no=>$data)

                                        <tr>
                                            <td>{{$no+1}}</td>
                                            <td>{{ $data->nama_barang }}</td>
                                            {{-- <td>{{ $data->satuan_barang }}</td> --}}
                                            <td>{{ $data->nama_kategori }}</td>
                                            <td>
                                                <a href="/namabarang/edit/{{ $data->id_nama_barang }}"
                                                    class="btn btn-warning" style="color:black">Edit</a>
                                                <a href="/namabarang/delete/{{ $data->id_nama_barang }}"
                                                    class="btn btn-danger" style="color:black">Hapus</a>
                                            </td>
                                        </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </>
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    @include('layouts.logoutModal')

    <!-- Bootstrap core JavaScript-->
    @include('layouts.script')

</body>

</html>