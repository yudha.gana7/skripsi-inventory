<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <title>Inventory Jago Sore | Data Satuan</title>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Data Satuan Barang</h1>
                    <p class="mb-4">Data wajib diupdate secara berkala agar tidak terjadi kesalahan data
                        <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"></div>
                                <div class="col-sm-12 col-md-6">
                                    <a href="{{ url('/satuan/tambah')}}"
                                        class=" btn btn-sm btn-primary shadow-sm float-right mb-3"><i
                                            class="fas fa-plus-square fa-sm text-white-50"></i> Tambah Data Satuan</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Satuan Barang</th>
                                            {{-- <th>Keterangan Satuan</th> --}}
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Satuan Barang</th>
                                            {{-- <th>Keterangan Satuan</th> --}}
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($satuan as $no=>$data)

                                        <tr>
                                            <td>{{$no+1}}</td>
                                            <td>{{ $data->satuan_barang }}</td>
                                            {{-- <td>{{ $data->keterangan_satuan }}</td> --}}
                                            <td>
                                                <a href="/satuan/edit/{{ $data->id_satuan }}" class="btn btn-warning"
                                                    style="color:black">Edit</a>
                                                <a href="/satuan/delete/{{ $data->id_satuan }}" class="btn btn-danger"
                                                    style="color:black">Hapus</a>
                                            </td>
                                        </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </>
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    @include('layouts.logoutModal')

    <!-- Bootstrap core JavaScript-->
    @include('layouts.script')

</body>

</html>