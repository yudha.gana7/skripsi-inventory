<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <title>Inventory Jago Sore | Edit Satuan Berat</title>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="container">
                                <!-- Nested Row within Card Body -->
                                <div class="col-lg-12">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Edit Barang</h1>
                                        </div>
                                        <form class="user" method="post"
                                            action="/satuan/update/{{ $satuan->id_satuan }}">
                                            {{ csrf_field() }}
                                            {{ method_field('PUT') }}

                                            <div class="form-group">
                                                <label>Nama Satuan</label>
                                                <input type="text" class="form-control " name="satuan_barang"
                                                    value="{{ $satuan->satuan_barang }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Keterangan Satuan</label>
                                                <input type="text" class="form-control " name="keterangan_satuan"
                                                    value="{{ $satuan->keterangan_satuan }}">
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <a class="btn btn-warning btn-user btn-block"
                                                        href=" {{url()->previous()}}">Batal</a>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <button type="submit" class="btn btn-success btn-user btn-block">
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    @include('layouts.logoutModal')

    <!-- Bootstrap core JavaScript-->
    @include('layouts.script')

</body>

</html>
