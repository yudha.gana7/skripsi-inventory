<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <title>Inventory Jago Sore | Data Pengguna</title>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Data User Terdaftar</h1>
                    <p class="mb-4">Data wajib diupdate secara berkala agar tidak terjadi kesalahan data
                        <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Nomor Handphone</th>
                                            <th>Alamat</th>
                                            {{-- <th>Username</th> --}}
                                            {{-- <th>Password</th> --}}
                                            <th>Level</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Nomor Handphone</th>
                                            <th>Alamat</th>
                                            {{-- <th>Username</th> --}}
                                            {{-- <th>Password</th> --}}
                                            <th>Level</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach ($user as $no=>$data)
                                        <tr>
                                            <td>{{$no+1}}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->nomor_hp }}</td>
                                            <td>{{ $data->alamat }}</td>
                                            {{-- <td>{{ $data->username }}</td> --}}
                                            {{-- <td>{{ $data->password }}</td> --}}
                                            <td>{{ $data->level }}</td>
                                            <td>
                                                <a href="/user/edit/{{ $data->id_user }}" class="btn btn-warning"
                                                    style="color:black">Edit</a>
                                                <a href="/user/delete/{{ $data->id_user }}" class="btn btn-danger"
                                                    style="color:black">Hapus</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    @include('layouts.logoutModal')

    <!-- Bootstrap core JavaScript-->
    @include('layouts.script')

</body>

</html>