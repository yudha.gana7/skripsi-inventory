<!-- <!DOCTYPE html>
<html>
<head>
 <title>Tutorial Membuat CRUD Pada Laravel - www.malasngoding.com</title>
</head>
<body>

 <a href="/product"> Kembali</a>

 <br/>
 <br/>

 <form action="/product/store" method="post">
  @csrf
  Nama Barang <input type="text" name="namabarang" required="required"> <br/>
  Kategori Barang <input type="number" name="kategori" required="required"> <br/>
  Lama Penyimpanan <input type="number" name="lamapenyimpanan" required="required"> <br/>
  Penginput Data <input type="text" name="user" required="required"> <br/>
    Harga Barang <input type="number" name="hargabarang" required="required"> <br/>
  <input type="submit" value="Simpan Data">
 </form>

</body>
</html>

 -->



<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <title>Inventory Jago Sore | Tambah Kategori</title>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="container">
                                <!-- Nested Row within Card Body -->
                                <div class="col-lg-12">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Tambah Kategori</h1>
                                        </div>

                                        @if($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif


                                        <form class="user" action="/kategori/store" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label>Nama Kategori</label>
                                                <input type="text" class="form-control" name="nama_kategori">
                                            </div>
                                            <div class="form-group">
                                                <label>Keterangan kategori</label>
                                                <input type="text" class="form-control" name="keterangan_kategori"
                                                    value="Tidak Ada Keterangan Kategori">
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <a class="btn btn-warning btn-user btn-block"
                                                        href=" {{url()->previous()}}">Batal</a>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <button type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Simpan Data">
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    @include('layouts.logoutModal')

    <!-- Bootstrap core JavaScript-->
    @include('layouts.script')

    {{-- @include('layouts.script2') --}}

</body>

</html>