<!DOCTYPE html>
<html>

<head>
    <title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
        table tr td,
        table tr th {
            font-size: 9pt;
        }
    </style>
    {{-- <center>
        <h5>Membuat Laporan PDF Dengan DOMPDF Laravel</h4>
            <h6><a target="_blank"
                    href="https://www.malasngoding.com/membuat-laporan-…n-dompdf-laravel/">www.malasngoding.com</a>
        </h5>
    </center> --}}

    <table width="100%">
        <tr>
            <td width="25" align="center"><img src="Tes.jpg" width="60%"></td>
            <td width="50" align="center">
                <h1>Gemscool Game Portal Pertama Indonesia</h1><br>
                <h2>Jakarta</h2>
            </td>
            <td width="25" align="center"><img src="Logo DN.jpg" width="100%"></td>
        </tr>
    </table>
    <hr>

    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Stok Barang</th>
                <th>Satuan</th>
                <th>Kategori</th>
                <th>Harga(Rp.)</th>
                <th>Tanggal Kadaluarsa</th>
                <th>Keterangan Simpan</th>
                {{-- <th>Data Diperbaharui Oleh</th> --}}
                {{-- <th>Diperbaharui</th> --}}
                {{-- <th>Aksi</th> --}}
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach($barang as $no=>$data)

            <tr>
                <td>{{$no+1}}</td>
                <td>{{ $data->nama_barang }}</td>
                <td>{{ $data->stok_barang,0 }}</td>
                <td>{{ $data->satuan_barang }}</td>
                <td>{{ $data->nama_kategori }}</td>
                <td>{{ number_format($data->harga_barang) }}</td>
                <td>{{ date('D, d M Y', strtotime($data->lama_penyimpanan)) }}</td>
                <td>{{ $data->keterangan_penyimpanan }}</td>
                {{-- <td>{{ $data->name }}</td> --}}
                {{-- <td>{{ date('d M Y H:i:s', strtotime($data->updated_at)) }}</td> --}}
                {{-- <td>
                    <div class="row">
                        <div class="col mb-1">
                            <a href="/product/edit/{{ $data->id_barang }}" class="btn btn-warning w-100"
                                style="color:black">Edit</a>
                        </div>
                        <div class="col">
                            <a href="/product/delete/{{ $data->id_barang }}" class="btn btn-danger w-100"
                                style="color:black">Hapus</a>
                        </div>
                    </div>
                </td> --}}
            </tr>

            @endforeach
        </tbody>
    </table>

</body>

</html>