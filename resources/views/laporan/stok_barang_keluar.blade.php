<!DOCTYPE html>
<html lang="en">

<head>
	@include('layouts.head')
	<title>Inventory Jago Sore | Barang Keluar</title>
</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		@include('layouts.sidebar')
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				@include('layouts.navbar')
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- Page Heading -->
					<!-- DataTales Example -->
					<div class="card shadow mb-4">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 col-md-6">
									<a href="/laporan/stok/print" class="btn btn-primary" target="_blank">CETAK PDF</a>
								</div>
								<div class="col-sm-12 col-md-6">
									<a href="{{ url('/product/tambah')}}"
										class=" btn btn-sm btn-primary shadow-sm float-right mb-3"><i
											class="fas fa-plus-square fa-sm text-white-50"></i> Tambah Produk</a>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Barang</th>
											<th>Stok Barang</th>
											<th>Satuan</th>
											<th>Kategori</th>
											<th>Harga(Rp.)</th>
											<th>Tanggal Kadaluarsa</th>
											<th>Keterangan Simpan</th>
											{{-- <th>Data Diperbaharui Oleh</th> --}}
											{{-- <th>Diperbaharui</th> --}}
											<th>Aksi</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th>No</th>
											<th>Nama</th>
											<th>Stok Barang</th>
											<th>Satuan</th>
											<th>Kategori</th>
											<th>Harga(Rp.)</th>
											<th>Tanggal Kadaluarsa</th>
											<th>Keterangan Simpan</th>
											{{-- <th>Data DiPerbaharui Oleh</th> --}}
											{{-- <th>Diperbaharui</th> --}}
											<th>Aksi</th>
										</tr>
									</tfoot>
									<tbody>
										@foreach($barang as $no=>$data)

										<tr>
											<td>{{$no+1}}</td>
											<td>{{ $data->nama_barang }}</td>
											<td>{{ $data->stok_barang,0 }}</td>
											<td>{{ $data->satuan_barang }}</td>
											<td>{{ $data->nama_kategori }}</td>
											<td>{{ number_format($data->harga_barang) }}</td>
											<td>{{ date('D, d M Y', strtotime($data->lama_penyimpanan)) }}</td>
											<td>{{ $data->keterangan_penyimpanan }}</td>
											{{-- <td>{{ $data->name }}</td> --}}
											{{-- <td>{{ date('d M Y H:i:s', strtotime($data->updated_at)) }}</td> --}}
											<td>
												<div class="row">
													<div class="col mb-1">
														<a href="/product/edit/{{ $data->id_barang }}"
															class="btn btn-warning w-100" style="color:black">Edit</a>
													</div>
													<div class="col">
														<a href="/product/delete/{{ $data->id_barang }}"
															class="btn btn-danger w-100" style="color:black">Hapus</a>
													</div>
												</div>
											</td>
										</tr>

										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						</>
					</div>
				</div>
				<!-- /.container-fluid -->
			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			@include('layouts.footer')
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	@include('layouts.logoutModal')

	<!-- Bootstrap core JavaScript-->
	@include('layouts.script')

</body>

</html>