<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <title>Inventory Jago Sore | Tambah Barang</title>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="container">
                                <!-- Nested Row within Card Body -->
                                <div class="col-lg-12">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Tambah Barang</h1>
                                        </div>

                                        @if($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif


                                        <form class="user" action="/transaksi-msk/store" method="post">
                                            @csrf
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <label>Nama Barang</label>
                                                    <select class="form-control" id="namabarang-option"
                                                        name="id_nama_barang">
                                                        @foreach ($namabarangs as $nmb)
                                                        <option value="{{ $nmb->id_nama_barang }}">
                                                            {{ $nmb->nama_barang }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Stok Barang</label>
                                                    <input type="number" class="form-control" name="stok_barang">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Satuan Barang</label>
                                                    <select class="form-control" id="satuan-option" name="id_satuan">
                                                        @foreach ($satuans as $idst)
                                                        <option value="{{ $idst->id_satuan }}">
                                                            {{ $idst->satuan_barang }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            {{-- <div class="form-group">
                                                <label>Keterangan Penyimpanan</label>
                                                <textarea rows="5" cols="50" class="form-control"
                                                    name="keterangan_penyimpanan"
                                                    value="Tidak Ada Keterangan Penyimpanan">
                                                </textarea>
                                            </div> --}}
                                            <div class="form-group">
                                                <label>Keterangan Penyimpanan</label>
                                                <input type="text" class="form-control" name="keterangan_penyimpanan"
                                                    value="Tidak Ada Keterangan Penyimpanan">
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    {{-- <label>Lama Penyimpanan</label>
                                                    <input type="date" class="form-control" name="lama_penyimpanan">
                                                    --}}
                                                    <label>Lama Penyimpanan</label>
                                                    <input type="date" class="form-control" name="lama_penyimpanan">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Harga Barang (Rp.)</label>
                                                    <input type="number" class="form-control" name="harga_barang">
                                                </div>
                                            </div>
                                            {{-- <div class="form-group">
                                                <div>
                                                    <label>Kategori Barang</label>
                                                    <select class="form-control" id="category-option"
                                                        name="id_kategori">
                                                        @foreach ($category as $ctgr)
                                                        <option value="{{ $ctgr->id_kategori }}">
                                                            {{ $ctgr->nama_kategori }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div> --}}
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" name="id_user"
                                                    value="{{Auth::id()}}" readonly>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <a class="btn btn-warning btn-user btn-block"
                                                        href=" {{url()->previous()}}">Batal</a>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <button type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Simpan Data">
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

            {{--
        </div> --}}
        <!-- End of Main Content -->

        <!-- Footer -->
        @include('layouts.footer')
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    @include('layouts.logoutModal')

    <!-- Bootstrap core JavaScript-->
    @include('layouts.script')


    {{-- @include('layouts.script2') --}}

</body>

</html>