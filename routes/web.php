<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SatuanController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LogActivityController;
use App\Http\Controllers\BarangMasukController;
use App\Http\Controllers\BarangKeluarController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\NamaBarangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route Auth (Login)
Route::get('/', function () {
    return view('Auth.login');
});

Route::get('/user', [UserController::class, 'index']);

Route::get('login', function (){
    return view('Auth.login');
})->name('login');

Route::post('/postlogin', [LoginController::class, 'postLogin'])->name('postlogin');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

//Route Auth (Register)
Route::get('/register', [LoginController::class, 'register'])->name('register');
Route::post('/postRegister', [LoginController::class, 'postRegister'])->name('postRegister');

//Route Group Level (Administrator)
Route::group(['middleware' => ['auth', 'ceklevel:administrator']], function (){
    //Route Dashboard
    Route::get('/dashboard', [DashboardController::class, 'index']);

    //Route Data Master (Kategori)
    Route::get('/kategori', [CategoryController::class, 'index']);
    Route::get('kategori/tambah', [CategoryController::class, 'tambah']);
    Route::post('kategori/store', [CategoryController::class, 'storedata']);
    Route::get('kategori/edit/{id}', [CategoryController::class, 'edit']);
    Route::put('kategori/update/{id}', [CategoryController::class, 'updatedata']);
    Route::get('kategori/delete/{id}', [CategoryController::class, 'delete']);

    //Route Data Master (Satuan)
    Route::get('/satuan', [SatuanController::class, 'index']);
    Route::get('satuan/tambah', [SatuanController::class, 'tambah']);
    Route::post('satuan/store', [SatuanController::class, 'storedata']);
    Route::get('satuan/edit/{id}', [SatuanController::class, 'edit']);
    Route::put('satuan/update/{id}', [SatuanController::class, 'updatedata']);
    Route::get('satuan/delete/{id}', [SatuanController::class, 'delete']);

    //Route Manajemen (User)
    Route::get('user/edit/{id}', [UserController::class, 'edit']);
    Route::put('user/update/{id}', [UserController::class, 'updatedata']);
    Route::get('user/delete/{id}', [UserController::class, 'delete']);

    //Route Manajemen (User Activity Log)
    Route::get('/logActivity', [logActivityController::class, 'index']);
    //  Route::get('/SelfLogActivity', [logActivityController::class, 'selfActivity']);
});

//Route Group Level (Administrator dan Pegawai)
Route::group(['middleware' => ['auth', 'ceklevel:administrator,pegawai']], function (){
    //Route Dashboard
    Route::get('/dashboard', [DashboardController::class, 'index']);

    //Route Data Master (Product)
    Route::get('/product', [ProductController::class, 'index']);
    // Route::get('product/tambah', [ProductController::class, 'tambah']);
    // Route::post('product/store', [ProductController::class, 'storedata']);
    Route::get('product/edit/{id}', [ProductController::class, 'edit']);
    Route::put('product/update/{id}', [ProductController::class, 'updatedata']);
    Route::get('product/delete/{id}', [ProductController::class, 'delete']);

    //Route Data Master (Nama Barang)
    Route::get('namabarang', [NamaBarangController::class, 'index']);
    Route::get('namabarang/tambah', [NamaBarangController::class, 'tambah']);
    Route::post('namabarang/store', [NamaBarangController::class, 'storedata']);
    Route::get('namabarang/edit/{id}', [NamaBarangController::class, 'edit']);
    Route::put('namabarang/update/{id}', [NamaBarangController::class, 'updatedata']);
    Route::get('namabarang/delete/{id}', [NamaBarangController::class, 'delete']);

    //Route Laporan
    Route::get('laporan/stok', [LaporanController::class, 'index']);
    Route::get('laporan/stok/print', [LaporanController::class, 'print']);

    //Route Log Activity
    Route::get('add-to-log', 'App\Http\Controllers\LogActivityController@myTestAddToLog');
    Route::get('log-activity', 'App\Http\Controllers\LogActivityController@logActivity');

    //Route Manajemen (Log Activity)
    Route::get('/SelfLogActivity', [logActivityController::class, 'selfActivity']);

    //Route Transaksi (Barang Masuk)
    Route::get('transaksi-msk', [BarangMasukController::class, 'index']);
    Route::get('transaksi-msk/tambah', [BarangMasukController::class, 'tambah']);
    Route::post('transaksi-msk/store', [BarangMasukController::class, 'storedata']);
    Route::get('transaksi-msk/edit/{id}', [BarangMasukController::class, 'edit']);
    Route::put('transaksi-msk/update/{id}', [BarangMasukController::class, 'updatedata']);
    Route::get('transaksi-msk/delete/{id}', [BarangMasukController::class, 'delete']);

     //Route Transaksi (Barang Keluar)
     Route::get('transaksi-klr', [BarangMasukController::class, 'index']);
     Route::get('transaksi-klr/tambah', [BarangMasukController::class, 'tambah']);
     Route::post('transaksi-klr/store', [BarangMasukController::class, 'storedata']);
     Route::get('transaksi-klr/edit/{id}', [BarangMasukController::class, 'edit']);
     Route::put('transaksi-klr/update/{id}', [BarangMasukController::class, 'updatedata']);
     Route::get('transaksi-klr/delete/{id}', [BarangMasukController::class, 'delete']);

});
