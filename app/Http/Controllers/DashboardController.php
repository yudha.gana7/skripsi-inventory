<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Http\Controllers\controller;


class DashboardController extends Controller
{
    public function index()
    {
        $pegawaicount = User::where('level', 'pegawai')->count();

        return view('dashboard', compact('pegawaicount'));
    }

    //count pegawai
    public function countPegawai(){
        $pegawaicount = User::where('level', 'pegawai')->count();

        return view('dashboard', compact('pegawaicount'));
    }
}
