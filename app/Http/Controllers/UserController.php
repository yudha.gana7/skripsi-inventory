<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\user;

class UserController extends Controller
{
    // index
    public function index()
    {
        $user = user::all();
        return view('user.user_data', ['user' => $user]);
    }
    //edit data
    public function edit($id)
    {
        $user = user::findOrFail($id);

        return view('user.user_edit', ['user' => $user]);
    }

    public function updatedata($id, Request $request)
    {
        $user = user::findOrfail($id);
        $user->update($request->all());

        \LogActivity::addToLog('Melakukan Edit Pada Tabel User');
        return redirect('/user');
        // dd($request->all());
    }
    //delete data
    public function delete($id)
    {
        $user =user::find($id);
        $user->delete();

        \LogActivity::addToLog('Menghapus Data Pada Tabel User');
        return redirect('/user');
    }
    //dropdown
    // public function create()
    // { {
    //         $category = kategori::all();
    //         return view('barang.create', compact('barang'));
    //     }

    // }
    // // tambah data
    // public function tambah()
    // {
    //     $category = kategori::all();
    //     return view('barang_tambah', compact('category'));
    // }
    // //     public function tambah()
    // // {
    // //     return view('barang_tambah');
    // // }
    // public function storedata(Request $request)
    // {
    //     barang::create([
    //         'nama_barang' => $request->namabarang,
    //         'stok_barang' => $request->stokbarang,
    //         'keterangan_penyimpanan' => $request->keteranganpenyimpanan,
    //         'lama_penyimpanan' => $request->lamapenyimpanan,
    //         'harga_barang' => $request->hargabarang,
    //         'id_kategori' => $request->kategori,
    //         'id_user' => $request->namapenginput,
    //     ]);

    //     return redirect('/product');
    // }
    // //edit data
    // public function edit($id)
    // {
    //     $barang = barang::find($id);
    //     return view('barang_edit', ['barang' => $barang]);
    // }

    // public function updatedata($id, Request $request)
    // {
    //     $barang = barang::find($id);
    //     $barang->nama_barang = $request->namabarang;
    //     $barang->stok_barang = $request->stokbarang;
    //     $barang->keterangan_penyimpanan = $request->keteranganpenyimpanan;
    //     $barang->lama_penyimpanan = $request->lamapenyimpanan;
    //     $barang->harga_barang = $request->hargabarang;
    //     $barang->id_kategori = $request->kategori;
    //     $barang->id_user = $request->namapenginput;
    //     $barang->save();
    //     return redirect('/product');
    // }
    // //delete data
    // public function delete($id)
    // {
    //     $barang = Barang::find($id);
    //     $barang->delete();
    //     return redirect('/product');
    // }
}

