<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\barang;
use App\Models\kategori;
use App\Models\user;
use App\Models\satuan;
use App\Models\barangMasuk;
use App\Models\namabarang;

class BarangMasukController extends Controller
{
    public function index()
    {

        // $barang_masuk = barangMasuk::join('nama_barang', 'nama_barang.id_nama_barang', '=', 'barang_masuk.id_nama_barang')
        //                 ->join('kategori', 'kategori.id_kategori', '=', 'nama_barang.id_kategori')
        //                 ->join('satuan', 'satuan.id_satuan', '=', 'barang_masuk.id_satuan')
        //                 ->join('user', 'user.id_user', '=', 'barang_masuk.id_user')
        //                 ->orderBy('created_at','DESC')
        //                 ->select('barang_masuk.*', 'barang_masuk.harga_barang', 'kategori.nama_kategori', 'satuan.satuan_barang', 'nama_barang.nama_barang', 'barang_masuk.id_satuan', 'nama_barang.id_kategori', 'user.name')
        //                 ->get();

        // $barang = barang::all();

        // return view('transaksi.barang_masuk', ['barang' => $barang, 'barang_masuk' => $barang_masuk]);
        // return view('transaksi.barang_masuk', ['barang_masuk' => $barang_masuk]);
        // $barang_masuk = barangMasuk::join('nama_barang', 'nama_barang.id_nama_barang', '=', 'barang_masuk.id_nama_barang')
        // ->join('satuan', 'satuan.id_satuan', '=', 'barang_masuk.id_satuan')
        // ->join('user', 'user.id_user', '=', 'barang_masuk.id_user')
        // ->join('kategori', 'kategori.id_kategori', '=', 'nama_barang.id_kategori')
        // // ->orderBy('barang.lama_penyimpanan','ASC')
        // ->get([ 'barang_masuk.id_barang_masuk', 'nama_barang.nama_barang', 'kategori.nama_kategori', 'barang_masuk.stok_barang','satuan.satuan_barang', 'barang_masuk.harga_barang', 'user.name', 'barang_masuk.created_at']);
        $barang_masuk = barangMasuk::join('nama_barang', 'nama_barang.id_nama_barang', '=', 'barang_masuk.id_nama_barang')
                        ->join('kategori', 'kategori.id_kategori', '=', 'nama_barang.id_kategori')
                        ->join('satuan', 'satuan.id_satuan', '=', 'barang_masuk.id_satuan')
                        ->select('barang_masuk.*','nama_barang.nama_barang', 'kategori.nama_kategori', 'satuan.satuan_barang', )
                        ->get();

        return view('transaksi.barang_masuk', ['barang_masuk' => $barang_masuk]);
    }
    // tambah data
    public function tambah()
    {
        $category = kategori::all();
        $satuans = satuan::all();
        $namabarangs = namabarang::all();
        // $namabarangs = namabarang::join('kategori', 'kategori.id_kategori', '=', 'nama_barang.id_kategori')
        //     ->join('satuan', 'satuan.id_satuan', '=', 'nama_barang.id_satuan')
        //     ->get([ 'nama_barang.id_nama_barang','nama_barang.nama_barang', 'satuan.satuan_barang', 'kategori.nama_kategori']);
            
        return view('transaksi.barang_masuk_tambah', ['category' => $category, 'satuans' => $satuans, 'namabarangs' => $namabarangs]);
    }

    public function storedata(Request $request)
    {
        $barangmasuk =  barangmasuk::create($request->all())->with(['success' => 'Berhasil Menambahkan Barang']);
        $barang =  barang::create($request->all())->with(['success' => 'Berhasil Menambahkan Barang']);

        \LogActivity::addToLog('Menambahkan Data Pada Tabel Nama Barang');
        // dd($request->all());

        return redirect('/transaksi-msk');
    }
}
