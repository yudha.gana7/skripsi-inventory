<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use illuminate\Support\Facades\Session;
use App\Models\barang;
use App\Models\kategori;
use App\Models\user;
use App\Models\satuan;
use App\Models\LogActivity;
use Illuminate\Support\Facades\Auth;

class LogActivityController extends Controller
{
    public function index()
    {
        $log = logActivity::join('user', 'user.id_user', '=', 'log_activities.id_user')
            ->get([ 'log_activities.id_activity','log_activities.activity', 'user.name', 'log_activities.created_at']);
        
        return view('logActivity.logActivity', ['log' => $log]);
    }

    public function selfActivity()
    {
        $id = Auth::id();

        $log = logActivity::join('user', 'user.id_user', '=', 'log_activities.id_user')
            ->where('user.id_user', Auth::id())
            ->get([ 'log_activities.id_activity','log_activities.activity', 'user.name', 'log_activities.created_at']);
        
        return view('logActivity.logActivity', ['log' => $log]);
    }
}
