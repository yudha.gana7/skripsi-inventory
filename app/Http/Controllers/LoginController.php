<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
class LoginController extends Controller
{
    public function postlogin (Request $request){
        // dd($request->all());

       $request->validate([
        'username' =>'required|string',
        'password' => 'required|min:4',
       ], [
        'username.required' => 'Username Tidak Boleh Kosong',
        'password.required' => 'Password Tidak Boleh Kosong',
        'password.min' => 'Password Minimal 4 Karakter',
       ]);

        if (Auth::attempt($request->only('username', 'password'))){

            \LogActivity::addToLog('User Telah Login Ke Sistem');
            return redirect('/dashboard');
        }
        return redirect ('login');

        // if($this->validate($request, [
        //     'username' =>'required|string',
        //     'password' => 'required|string',
        // ]);

        // if (Auth::attempt($request->only('username', 'password'))){

        //     \LogActivity::addToLog('User Telah Login Ke Sistem');
        //     return redirect('/dashboard');
        // }
        // return redirect ('login');
    }

    public function logout (){
        Auth::logout();
        Session::flush();
        \LogActivity::addToLog('User Telah Logout Dari Sistem');
        return redirect('/');
    }

    public function register(){
        return view('Auth.register');
    }

    public function postRegister(Request $request){
        // dd($request->all());

        $request->validate([
            'name' =>'required|alpha',
            'nomor_hp' => [
                'required', 
                'regex:/^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/',
                'min:10',
                'numeric'
              ],
            'alamat' => 'required',
            'username' => 'required',
            'password' => [
                'required',
                'string',
                Password::min(8)
                    ->mixedCase()
                    ->letters()
                    ->numbers()
                    ->symbols()
                    ->uncompromised()],
            'password_confirmation'  => 'required|same:password',
           ], [
            'name.required' => 'Nama Tidak Boleh Kosong',
            'name.alpha' => 'Nama Hanya Boleh Diisi Huruf',
            'nomor_hp.required' => 'Nomor Hp Tidak Boleh Kosong',
            'nomor_hp.regex' => 'Nomor Hp Harus Sesuai Format (+62/62/08)',
            'nomor_hp.min' => 'Nomor Hp Kurang Dari 10 Karakter',
            'nomor_hp.numeric' => 'Nomor Hp Harus Berisi Angka',
            'alamat.required' => 'Alamat Tidak Boleh Kosong',
            'username.required' => 'Username Tidak Boleh Kosong',
            'password.min' => 'Password Kurang Dari 8 Karakter',
            'password_confirmation.same' => 'Password Tidak Sama',
            'password_confirmation.required' => 'Password Konfirmasi Tidak Boleh Kosong',
           ]);

        User::create([
            'name' => $request->name,
            'nomor_hp' => $request->nomor_hp,
            'alamat' => $request->alamat,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'level' => 'pegawai',
            'remember_token' => Str::random(60),
        ]);

        \LogActivity::addToLog('User Berhasil Mendaftar Ke Sistem');
        return redirect('login');

    }




//     public function login()
//     {
//         if (Auth::check()){
//             return redirect('home');
//         }else{
//             return view('login');
//         }
//     }

//     public function actionlogin(Request $request)
//     {
//         $data = [
//             'username' => $request->input('username'),
//             'password' => $request->input('password'),
//         ];

//         if (Auth::Attempt($data)){
//             return redirect('/');
//         }else{
//             Session::flash('error', 'Email atau Password Salah');
//             return redirect('/');
//         }
//     }

//     public function actionlogout()
//     {
//         Auth::logout();
//         return redirect('/');
//     }
}
