<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use illuminate\Support\Facades\Session;
use App\Models\barang;
use App\Models\kategori;
use App\Models\user;
use App\Models\satuan;
use App\Models\NamaBarang;

class ProductController extends Controller
{

    // index
    public function index()
    {
        $barang = barang::where('stok_barang', '>', 0)
        ->join('nama_barang', 'nama_barang.id_nama_barang', '=', 'barang.id_nama_barang')
        ->join('satuan', 'satuan.id_satuan', '=', 'barang.id_satuan')
        ->join('user', 'user.id_user', '=', 'barang.id_user')
        ->join('kategori', 'kategori.id_kategori', '=', 'nama_barang.id_kategori')
        ->orderBy('barang.lama_penyimpanan','ASC')
        ->get([ 'barang.id_barang','nama_barang.nama_barang', 'kategori.nama_kategori', 'barang.stok_barang', 'barang.keterangan_penyimpanan', 'barang.lama_penyimpanan', 'satuan.satuan_barang', 'barang.harga_barang', 'user.name', 'barang.updated_at']);

        // $barang = barang::join('user', 'user.id_user', '=', 'barang.id_user')
        //     // ->join('kategori', 'kategori.id_kategori', '=', 'barang.id_kategori')
        //     ->join('nama_barang', 'nama_barang.id_nama_barang', '=', 'barang.id_nama_barang')
        //     ->join('satuan', 'satuan.id_satuan', '=', 'barang.id_satuan')
        //     ->orderBy('barang.lama_penyimpanan','ASC')
        //     ->get([ 'barang.id_barang','nama_barang.nama_barang', 'barang.stok_barang', 'barang.keterangan_penyimpanan', 'barang.lama_penyimpanan', 'satuan.satuan_barang', 'barang.harga_barang', 'user.name', 'barang.updated_at']);

        // $barang = barang::all();
        // $user = user::all();
        return view('barang.inventory', ['barang' => $barang]);
    }

    // // tambah data
    // public function tambah()
    // {
    //     $category = kategori::all();
    //     $satuans = satuan::all();
    //     return view('barang.barang_tambah', ['category' => $category, 'satuans' => $satuans]);
    // }

    // public function storedata(Request $request)
    // {
    //     // $validated = $request->validate([
    //         // 'nama_barang' => 'required',
    //     //     'stok_barang' => 'required',
    //     //     'keterangan_penyimpanan' => 'required',
    //     //     'lama_penyimpanan' => 'required',
    //     //     'harga_barang' => 'required',
    //     //     'id_satuan' => 'required',
    //     //     'id_kategori' => 'required',
    //     //     'id_user' => 'required',
    //     // ]);


    //     $Barang =  NamaBarang::create($request->all())->with(['success' => 'Berhasil Menambahkan Barang']);

    //     \LogActivity::addToLog('Menambahkan Data Pada Tabel Barang');
    //     // dd($request->all());

    //     return redirect('/product');
    // }
    //edit data
    // public function edit($id)
    // {
    //     $barang = barang::findOrFail($id);
    //     $category = kategori::all();
    //     $satuans = satuan::all();

    //     return view('barang.barang_edit', ['barang' => $barang, 'category' => $category, 'satuans' => $satuans]);
    // }

    // public function updatedata($id, Request $request)
    // {
    //     $barang = barang::findOrfail($id);

    //     $barang->update($request->all());

    //     \LogActivity::addToLog('Melakukan Edit Pada Tabel Barang');
    //     return redirect('/product');
    // }

    // //delete data
    // public function delete($id)
    // {
    //     $barang = barang::find($id);
    //     $barang->delete();

    //     \LogActivity::addToLog('Menghapus Data Pada Tabel Barang');
    //     return redirect('/product');
    // }
}

