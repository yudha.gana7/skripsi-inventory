<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\kategori;

class CategoryController extends Controller
{
    public function index()
    {
        $kategori = kategori::all();
        return view('kategori.kategori_data', ['kategori' => $kategori]);
    }

    //tambah data
    public function tambah()
    {
        return view('kategori.kategori_tambah');
    }

    public function storedata(Request $request)
    {
        $Kategori =  kategori::create($request->all())->with(['success' => 'Pesan Berhasil']);

        \LogActivity::addToLog('Menambahkan Data Pada Tabel Kategori');
        return redirect('/kategori');
        // dd($request->all());
    }

    //edit data
    public function edit($id)
    {
        $kategori = kategori::findOrFail($id);
        return view('kategori.kategori_edit', ['kategori' => $kategori]);
    }

    public function updatedata($id, Request $request)
    {
        $kategori = kategori::findOrfail($id);
        $kategori->update($request->all());

        \LogActivity::addToLog('Melakukan Edit Pada Tabel Kategori');
        return redirect('/kategori');
    }
    //delete data
    public function delete($id)
    {
        $kategori = kategori::find($id);
        $kategori->delete();

        \LogActivity::addToLog('Menghapus Data Pada Tabel Kategori');
        return redirect('/kategori');
    }
}
