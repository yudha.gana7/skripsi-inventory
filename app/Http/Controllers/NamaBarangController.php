<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use illuminate\Support\Facades\Session;
use App\Models\barang;
use App\Models\kategori;
use App\Models\user;
use App\Models\satuan;
use App\Models\namabarang;

class NamaBarangController extends Controller
{
    public function index()
    {
        $namabarang = namabarang::join('kategori', 'kategori.id_kategori', '=', 'nama_barang.id_kategori')
            ->select([ 'nama_barang.*', 'kategori.nama_kategori'])
            ->get();

        return view('NamaBarang.nama_barang', ['namabarang' => $namabarang]);
    }

     // tambah data
     public function tambah()
     {
         $category = kategori::all();
         $satuans = satuan::all();
         return view('NamaBarang.barang_tambah', ['category' => $category, 'satuans' => $satuans]);
     }
 
     public function storedata(Request $request)
     {
        $request->validate([
            'nama_barang' =>'required|alpha',
            'id_kategori' => 'required|numeric',
            ], [
            'nama_barang.required' => 'Nama Barang Tidak Boleh Kosong',
            'id_kategori.required' => 'Nomor Hp Tidak Boleh Kosong',
            'id_kategori.numeric' => 'Bukan Kategori Yang Valid',
           ]);
         $namabarang =  namabarang::create($request->all())->with(['success' => 'Berhasil Menambahkan Nama Barang']);
 
         \LogActivity::addToLog('Menambahkan Data Pada Tabel Nama Barang');
         // dd($request->all());
 
         return redirect('/namabarang');
     }

    //edit data
    public function edit($id)
    {
        $namabarang = namabarang::findOrFail($id);
        $category = kategori::all();
        $satuans = satuan::all();

        return view('NamaBarang.nama_barang_edit', ['nama_barang' => $namabarang, 'category' => $category, 'satuans' => $satuans]);
    }

    public function updatedata($id, Request $request)
    {
        $namabarang = namabarang::findOrfail($id);
        $namabarang->update($request->all());

        \LogActivity::addToLog('Melakukan Edit Pada Tabel Nama Barang');
        return redirect('/namabarang');
    }
    
    //delete data
     public function delete($id)
     {
         $namabarang =namabarang::find($id);
         $namabarang->delete();

         \LogActivity::addToLog('Menghapus Data Pada Tabel Nama Barang');
         return redirect('/namabarang');
     }
}
