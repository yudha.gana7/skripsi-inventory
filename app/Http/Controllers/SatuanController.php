<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\satuan;

class SatuanController extends Controller
{
    public function index()
    {
        $satuan = satuan::all();
        return view('satuanData.satuan_data', ['satuan' => $satuan]);
    }

        //tambah data
        public function tambah()
        {
            return view('satuanData.satuan_tambah');
        }

        public function storedata(Request $request)
        {
            $Satuan =  satuan::create($request->all())->with(['success' => 'Pesan Berhasil']);

            \LogActivity::addToLog('Menambahkan Data Pada Tabel Satuan');
            return redirect('/satuan');
            // dd($request->all());
        }

        //edit data
        public function edit($id)
        {
            $satuan = satuan::findOrFail($id);
            return view('satuanData.satuan_edit', ['satuan' => $satuan]);
        }

        public function updatedata($id, Request $request)
        {
            $satuan = satuan::findOrfail($id);
            $satuan->update($request->all());

            \LogActivity::addToLog('Melakukan Edit Pada Tabel Satuan');
            return redirect('/satuan');
        }
        //delete data
        public function delete($id)
        {
            $satuan =satuan::find($id);
            $satuan->delete();

            \LogActivity::addToLog('Menghapus Data Pada Tabel Barang');
            return redirect('/satuan');
        }
}
