<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use illuminate\Support\Facades\Session;
use App\Models\barang;
use App\Models\kategori;
use App\Models\user;
use App\Models\satuan;
use PDF;

class LaporanController extends Controller
{
    public function index()
    {
        // $barang = barang::join('user', 'user.id_user', '=', 'barang.id_user')
        //     ->join('kategori', 'kategori.id_kategori', '=', 'barang.id_kategori')
        //     ->join('satuan', 'satuan.id_satuan', '=', 'barang.id_satuan')
        //     // ->count('barang.stok_barang')
        //     ->get([ 'barang.id_barang','barang.nama_barang', 'barang.stok_barang', 'barang.keterangan_penyimpanan', 'barang.lama_penyimpanan', 'satuan.satuan_barang', 'barang.harga_barang', 'kategori.nama_kategori', 'user.name', 'barang.updated_at']);

        $barang = barang::where('stok_barang', '>', 0)
        ->join('nama_barang', 'nama_barang.id_nama_barang', '=', 'barang.id_nama_barang')
        ->join('satuan', 'satuan.id_satuan', '=', 'barang.id_satuan')
        ->join('user', 'user.id_user', '=', 'barang.id_user')
        ->join('kategori', 'kategori.id_kategori', '=', 'nama_barang.id_kategori')
        ->orderBy('barang.id_nama_barang','ASC')
        ->get([ 'barang.id_barang','nama_barang.nama_barang', 'kategori.nama_kategori', 'barang.stok_barang', 'barang.keterangan_penyimpanan', 'barang.lama_penyimpanan', 'satuan.satuan_barang', 'barang.harga_barang', 'user.name', 'barang.updated_at']);

        return view('laporan.stok_barang', ['barang' => $barang]);
    }

    public function print()
    {
        $barang = barang::join('user', 'user.id_user', '=', 'barang.id_user')
        ->join('kategori', 'kategori.id_kategori', '=', 'barang.id_kategori')
        ->join('satuan', 'satuan.id_satuan', '=', 'barang.id_satuan')
        // ->count('barang.stok_barang')
        ->get([ 'barang.id_barang','barang.nama_barang', 'barang.stok_barang', 'barang.keterangan_penyimpanan', 'barang.lama_penyimpanan', 'satuan.satuan_barang', 'barang.harga_barang', 'kategori.nama_kategori', 'user.name', 'barang.updated_at']);
 
    	$pdf = PDF::loadview('laporan.stok_barang_pdf',['barang'=>$barang]);
    	return $pdf->download('stock-barang-pdf.pdf');
    }

    public function laporanMasuk()
    {
        $barang_masuk = barangMasuk::join('barang', 'barang.id_barang', '=', 'barang_masuk.id_barang')
                        ->join('kategori', 'kategori.id_kategori', '=', 'barang.id_kategori')
                        ->join('satuan', 'satuan.id_satuan', '=', 'barang.id_satuan')
                        ->orderBy('created_at','ASC')
                        ->select('barang_masuk.*', 'kategori.nama_kategori', 'satuan.satuan_barang', 'barang.stok_barang', 'barang.harga_barang', 'barang.nama_barang')
                        ->get();

        $barang = barang::all();

        return view('laporan.stok_barang_masuk', ['barang' => $barang, 'barang_masuk' => $barang_masuk]);
    }
}
