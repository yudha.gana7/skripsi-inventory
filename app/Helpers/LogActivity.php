<?php
namespace App\Helpers;
use Request;
use App\Models\LogActivity as LogActivityModel;


class LogActivity
{


    public static function addToLog($activity)
    {
      $log = [];
      $log['activity'] = $activity;
      $log['id_user'] = auth()->check() ? auth()->user()->id_user : 1;
      LogActivityModel::create($log);
    }


    public static function logActivityLists()
    {
      return LogActivityModel::latest()->get();
    }


}