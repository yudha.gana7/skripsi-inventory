<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class user extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = "user";

    protected $primaryKey = 'id_user';

    protected $fillable = [
        'name',
        'nomor_hp',
        'alamat',
        'username',
        'password',
        'level',
        'created_at',
        'updated_at'
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function barang()
    {
        return $this->hasMany('App\Models\barang');
    }
}


//adding for register
