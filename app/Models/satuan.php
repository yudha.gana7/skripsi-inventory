<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class satuan extends Model
{
    protected $table = "satuan";

    protected $primaryKey = 'id_satuan';

    protected $fillable = [
        'satuan_barang',
        'created_at',
        'updated_at'
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function barang()
    {
        return $this->hasMany(Barang::class);
    }
}
