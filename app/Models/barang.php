<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class barang extends Model
{
    protected $table = "barang";

    protected $primaryKey = 'id_barang';

    protected $fillable = [
        'id_nama_barang',
        'stok_barang',
        'keterangan_penyimpanan',
        'lama_penyimpanan',
        'harga_barang',
        'id_satuan',
        'id_user',
        'created_at',
        'updated_at'
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function user()
    {
        return $this->belongsTo('App\Models\user');
    }
    public function satuan()
    {
        return $this->belongsTo('App\Models\satuan');
    }
}
