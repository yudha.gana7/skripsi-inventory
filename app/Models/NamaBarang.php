<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class namabarang extends Model
{
    use HasFactory;
    protected $table = 'nama_barang';

    protected $primaryKey = 'id_nama_barang';

    protected $fillable = [
        'nama_barang',
        'id_kategori',
        'created_at',
        'updated_at'
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    
    public function barang()
    {
        return $this->hasMany(Barang::class);
    }
}
